**************************************
Sphinx Demo and Python 3.11 Features
**************************************

This project is about the major new features of Python 3.11 and demonstrates
how to use Sphinx to create documentation about those new features.

Sphinx
==========

The file **Sphinx Demo.rst** in docs/source directory explains how to use
Sphinx and its many features.  This reStructured Text file shows how to
build Sphinx documentation from scratch (i.e. an empty project).

Some of the features demonstrated are:

- using ``sphinx-quickstart`` to create the sphinx framework
- adding reStructured Text and MarkDown text files of documentation
- installing Sphinx extensions
- extracting documentation from code
- automatically creating indexes of packages,modules, classes methods and
  functions

- including option to view the source code for the classes, methods and
  functions
- samples of various graphic elements that can be included in Sphinx
  documentation including:

  - various image formats (png, jpg, svg, etc.)
  - images generated from UML text
- how to output in multiple formats including HTML, LaTeX, PDF and many more

Python 3.11 New Features
============================

The file **New Features.rst** in docs/source discusses the new features of
Python 3.11.  This may be more easily viewed in the Sphinx documentation
once generated.  There is a module in the src directory that demonstrates
somme of the new Python features.

Other Project Information
============================

Contributions
---------------

Contributers are welcome!  Please enhance this project as desired and submit
pull requests.

Licensing
-------------------

Copyright and licensing is covered in the file LICENSE.txt.