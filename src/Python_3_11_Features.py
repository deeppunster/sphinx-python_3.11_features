"""
Python_3_11_Features.py - Show off new Python 3.11 features
"""

import logging
import logging.config
import yaml  # from PyYAML library
from logging import getLogger, debug, error
from pathlib import Path
from typing import Any, Union, Optional

__author__ = 'Travis Risner'
__project__ = 'Python3.11'
__creation_date__ = '02/13/2023'
# Copyright 2023 by Travis Risner - MIT License

log = None
class MyExceptionGroup(ExceptionGroup):
    """
    Used by pep654 - Exception Groups
    """
    def __new__(cls, message, excs, errcode):
        """
        Override the construction of an exeception group
        :param message: message from exception
        :param excs: the exception type
        :param errcode: error code returned by the exception
        """
        obj = super().__new__(cls, message, excs)
        obj.errcode = errcode
        return obj

    def derive(self, excs):
        """
        Override the derivation of one exception out of the group
        :param excs: the exception
        :return: incorporate the execption into the exception group
        """
        return MyExceptionGroup(self.message, excs, self.errcode)

class Python_311_FeaturesClass:
    """
    Python_3.11_FeaturesClass - Show off new Python 3.11 features
    """

    def __init__(self):
        pass

    def run_P311(self):
        """
        Top method for running Show off new Python 3.11 features.

        :return:
        """
        print('Running Python_311_FeaturesClass')
        self.pep654_example()
        self.pep678_example()
        return

    def pep654_example(self):
        """
        Example of PEP 654: Exception Groups and except*

        :return:
        """
        # define an exception group
        eg = MyExceptionGroup("eg", [TypeError(1), ValueError(2)], 42)

        try:
            """ Code that potentially thows some exceptions """
            pass
        except * TypeError:
            pass
        except * ValueError:
            pass

        match, rest = eg.split(ValueError)
        print(f'match: {match!r}: {match.errcode}')
        # match: MyExceptionGroup('eg', [ValueError(2)], 42): 42
        print(f'rest: {rest!r}: {rest.errcode}')
        # rest: MyExceptionGroup('eg', [TypeError(1)], 42): 42

        return

    def pep678_example(self):
        """
        Example of PEP 678: Adding notes to exceptions
        :return:
        """
        # capture and exception and add a note\
        try:
            raise TypeError('bad type')
        except Exception as e:
            e.add_note('Add some information')
            raise
        """ Notes can also be added to exception groups """



class Main:
    """
    Main class to start things rolling.
    """

    def __init__(self):
        """
        Get things started.
        """
        self.P311 = None
        return

    def run_P311(self):
        """
        Prepare to run Show off  new Python 3.11 features.

        :return:
        """
        self.P311 = Python_311_FeaturesClass()
        debug('Starting up P311')
        self.P311.run_P311()
        return

    @staticmethod
    def start_logging(work_dir: Path, debug_name: str):
        """
        Establish the logging for all the other scripts.

        :param work_dir:
        :param debug_name:
        :return: (nothing)
        """

        # Set flag that no logging has been established
        logging_started = False

        # find our working directory and possible logging input file
        _workdir = work_dir
        _logfilename = debug_name

        # obtain the full path to the log information
        _debugConfig = _workdir / _logfilename

        # verify that the file exists before trying to open it
        if Path.exists(_debugConfig):
            try:
                #  get the logging params from yaml file and instantiate a log
                with open(_logfilename, 'r') as _logdictfd:
                    _logdict = yaml.load(_logdictfd, Loader=yaml.SafeLoader)
                logging.config.dictConfig(_logdict)
                logging_started = True
            except Exception as xcp:
                print(
                    f'The file {_debugConfig} exists, but does not contain '
                    f'appropriate logging directives.'
                )
                raise ValueError('Invalid logging directives.')
        else:
            print(
                f'Logging directives file {_debugConfig} either not '
                f'specified or not found'
            )

        if not logging_started:
            # set up minimal logging
            _logfilename = 'debuginfo.txt'
            _debugConfig = _workdir / _logfilename
            logging.basicConfig(
                filename='debuginfo.txt', level=logging.INFO, filemode='w'
            )
            print(f'Minimal logging established to {_debugConfig}')

        # start logging
        global log
        log = logging.getLogger(__name__)
        logging.info(f'Logging started: working directory is {_workdir}')
        return


if __name__ == '__main__':
    workdir = Path.cwd()
    debug_file_name = 'debug_info.yaml'
    main = Main()
    main.start_logging(workdir, debug_file_name)
    main.run_P311()

# EOF
