Image Examples
########################

PNG Images
============

Deployment Overview
-------------------------

.. image:: Images/DeploymentOverview.png

SVG Images
====================

Table Relationships
--------------------------

.. image:: Images/InventoryProjectModelsGrouped.svg

UML Images
==================


..  uml:: Images/CheckinInventoryUseCase.puml
    :caption: **Checkin Inventory Use Case**

..  uml:: Images/CheckinSequenceDiagram.puml
    :caption: **Checkin Inventory Sequence Diagram**
