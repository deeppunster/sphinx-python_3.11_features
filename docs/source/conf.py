# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here.
from pathlib import Path
import sys

path_to_conf = Path(__file__)
# print(f'{path_to_conf=}, {type(path_to_conf)}')
path_to_project = path_to_conf.parents[2]
path_to_src = path_to_project / 'src'
print(f'{path_to_src}')

sys.path.insert(0, path_to_src.as_posix())

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Python 3.11 New Features'
copyright = '2023, Travis Risner'
author = 'Travis Risner'
release = '0.1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.duration',
    'myst_parser',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.viewcode',
    'sphinx.ext.imgconverter',
    'sphinxcontrib.plantuml',
    'sphinx_rtd_theme',
]

templates_path = ['_templates']
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

autodoc_default_options = {
    'members': True,
    'undoc-members': True,
    'member-order': 'bysource',
    'special-members': '__init__',
}

# add markdown source files to possible documentation
source_suffix = [
        '.rst',
        '.md',
]

# path to possible processing tool to convert images from one format to another
image_converter = '/usr/bin/convert'


# EOF
