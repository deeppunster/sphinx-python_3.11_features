******************************************************************************
Python 3.11 Feature Excerpts
******************************************************************************


Overview
==============================================================================

`Python 3.11 Docs <https://docs.python.org/3/>`_

`Python 3.11 New Features <https://docs.python.org/3/whatsnew/3.11.html>`_

Fine-Grained Error Locations In Tracebacks
===========================================================

Examples
------------

Now shows which object has NoneType

.. code::

    Traceback (most recent call last):
      File "distance.py", line 11, in <module>
        print(manhattan_distance(p1, p2))
              ^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "distance.py", line 6, in manhattan_distance
        return abs(point_1.x - point_2.x) + abs(point_1.y - point_2.y)
                               ^^^^^^^^^
    AttributeError: 'NoneType' object has no attribute 'x'

Now shows more information for deeply nested calls

.. code::

    Traceback (most recent call last):
      File "query.py", line 37, in <module>
        magic_arithmetic('foo')
      File "query.py", line 18, in magic_arithmetic
        return add_counts(x) / 25
               ^^^^^^^^^^^^^
      File "query.py", line 24, in add_counts
        return 25 + query_user(user1) + query_user(user2)
                    ^^^^^^^^^^^^^^^^^
      File "query.py", line 32, in query_user
        return 1 + query_count(db, response['a']['b']['c']['user'], retry=True)
                                   ~~~~~~~~~~~~~~~~~~^^^^^
    TypeError: 'NoneType' object is not subscriptable

Shows better help for complex arithmetic expressions

.. code::

    Traceback (most recent call last):
      File "calculation.py", line 54, in <module>
        result = (x / y / z) * (a / b / c)
                  ~~~~~~^~~
    ZeroDivisionError: division by zero

`See PEP 657 for more information <https://docs.python.org/3/whatsnew/3.11
.html#whatsnew311-pep657>`_

New Syntax Features
====================

Exception Groups and except *
----------------------------------------

`Pep 654: <https://docs.python.org/3/whatsnew/3.11.html#whatsnew311-pep654>`_


New Built-in Features
=======================

Exceptions can be enriched with notes
----------------------------------------

`PEP 678: <https://docs.python.org/3/whatsnew/3.11.html#whatsnew311-pep678>`_

New Standard Library Module
================================

TOML Support
----------------------

Support for TOML now included in the standard library.

`PEP 680: tomllib <https://peps.python.org/pep-0680/>`_

This library is a replacement for `toml`.

Performance Features
=====================

Python 3.11 is between 10% to 60% faster than 3.10 with an average of about
25%.  More informmation about how this is accompllished is available at
`Faster CPython <https://docs.python.org/3/whatsnew/3.11
.html#whatsnew311-faster-cpython>`_.

Other New Features
======================

New Type Hint Features
-------------------------

Variadic Type Hints - see `TypeVarTuple <https://docs.python
.org/3/library/typing.html#typing.TypeVarTuple>`_

Marking individual TypedDict items as required or not-required
---------------------------------------------------------------

See `Using Required and NotRequired <https://docs.python.org/3/whatsnew/3.11
.html#whatsnew311-pep655>`_

Self Type
------------------

Useful in class-tailoring code.  See the `self type <https://docs.python
.org/3/whatsnew/3.11.html#whatsnew311-pep673>`_ documentation for details.

Arbitrary literal string type
--------------------------------

Allows a parameter to be marked so that it can only be a literal string --
not a variable.  See the `documentation <https://docs.python.org/3/whatsnew/3.11.html#whatsnew311-pep675>`_ for more information.

Data class transforms
-------------------------

Tells the IDE that a class has dataclass-like behaviors.  See `the
documentation <https://docs.python.org/3/whatsnew/3
.11.html#whatsnew311-pep681>`_ for more information.

Other Changes
===================

There are other changes and deprecations with this release.  See `Python 3
.11 New Features <https://docs.python.org/3/whatsnew/3.11.html>`_ for details.